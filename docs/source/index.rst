.. N-puzzle documentation master file, created by
   sphinx-quickstart on Thu Nov 19 18:22:04 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to N-puzzle's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   packages

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
