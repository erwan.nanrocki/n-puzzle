N-puzzle's Packages
===================

npuzzle module
--------------
.. automodule::
   npuzzle.node
   :members:
   :undoc-members:
   :private-members:
   :special-members:
   :inherited-members:

constant module
---------------
.. automodule::
   npuzzle.constant
   :members:
   :undoc-members:
   :private-members:
   :special-members:
   :inherited-members:

heuristic module
----------------
.. automodule::
   npuzzle.heuristic
   :members:
   :undoc-members:
   :private-members:
   :special-members:
   :inherited-members:

functions module
----------------
.. automodule::
   npuzzle.functions
   :members:
   :undoc-members:
   :private-members:
   :special-members:
   :inherited-members:
