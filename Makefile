.PHONY: default pip exe pylint doc pytest pytest_vvx test

default:
	@echo "make pip        : download and install all packages"
	@echo "make exe        : install and create executable"
	@echo "make pylint     : run pylint"
	@echo "make doc        : update the doc"
	@echo "make pytest     : run pytest"
	@echo "make pytest_vv  : run pytest -vv"
	@echo "make pytest_vvx : run pytest -vvx"
	@echo "make test       : run pylint and pytest"

clean:
	@rm -rfv ./dist/
	@rm -rfv ./build/
	@rm -fv npuzzle.html
	@rm -fv app.spec
	@rm -fv app.exe
	@rm -fv app

pip:
	pip install -r requirements.txt
	cat requirements.txt

exe:
	pyinstaller --onefile ./app.py

pylint:
	python -m pylint ./app.py
	python -m pylint ./npuzzle/

doc:
	cd ./docs/ && make html

pytest:
	python -m pytest ./npuzzle/

pytest_vv:
	python -m pytest ./npuzzle/ -vv

pytest_vvx:
	python -m pytest ./npuzzle/ -vvx

test:
	make pylint
	make pytest
